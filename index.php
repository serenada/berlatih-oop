<?php
    require_once ('animal.php');
    require_once ('ape.php');
    require_once ('frog.php');

    $sheep = new Animal("shaun");  
    echo "Nama: " . $sheep->name . "<br>"; // "shaun"
    echo "Jumlah kaki: " . $sheep->legs . "<br>"; // 2
    echo "Berdarah dingin: ";
    echo var_dump ($sheep->cold_blooded)  . "<br><br>";// false
    
    $sungokong = new Ape("kera sakti");
    echo "Nama: " . $sungokong->name . "<br>";
    echo "Jumlah kaki: " . $sungokong->legs . "<br>";
    echo "Teriakan: ";
    echo $sungokong->yell() . "<br> <br>"; // "Auooo"

    $kodok = new Frog("buduk");
    echo "Nama: " . $kodok->name . "<br>";
    echo "Jumlah kaki: " . $kodok->legs . "<br>";
    echo "Lompatan: ";
    echo $kodok->jump(); // "hop hop"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>